package lab6PA;

import java.util.*;

public class Comoara {

	static char[][] Matrice = {{'.', '.', '.', '#', '.', '.', '.', '.'}, 
		{'#', '.', '.', '#', '.', '.', '#', '.'},
		{'.', '#', '#', '.', 'P', '.', '.', '#'},
		{'.', '.', '#', '.', '#', '.', '#', '.'},
		{'.', '.', '.', '.', '.', '.', '.', '.'},
		{'.', '.', '.', '.', '.', '.', '.', '.'},
		{'#', '#', '#', '.', '.', '.', '#', '#'},
		{'.', '.', 'P', '.', '.', '.', '.', '.'}};
	static int  [][] MatriceFinala = {{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0}};

	static int dimension = 8;



	public static void main(String[] args){
		class Killer{
			public int row;
			public int collumn;
			public Killer(int x , int y){
				this.row = x ;
				this.collumn = y;
			}

		}
		//LA BFS NE Folosim de FIFO
		Queue<Killer> listaDrumuri = new LinkedList<Killer>();
		Queue<Killer> listaPaznici = new LinkedList<Killer>();

		/*
		 * Pun intai de toate cazurile initiale in vectorul final
		 */
		for ( int i =0 ;i < dimension ; i++){
			for ( int  j = 0 ; j< dimension ; j++){
				if (Matrice[i][j]== '#'){
					//casuta inaccesibila
					MatriceFinala[i][j] = -2;
				}
				else if (Matrice[i][j] == '.'){
					//casuta buna
					MatriceFinala[i][j] = -1;
				}
				else if (Matrice[i][j] == 'P'){
					//casuta cu un killer pe ea
					MatriceFinala[i][j] = 0;
					Killer nou = new Killer(i,j);
					listaPaznici.add(nou);
				}
			}

		}
		
		while (!listaPaznici.isEmpty() && listaPaznici.size()>=1){
			listaDrumuri.add(listaPaznici.poll());

			while (!listaDrumuri.isEmpty()&&listaDrumuri.size()>=1){
				Killer curent = listaDrumuri.poll();//scot din varful stivei si sterg din varful stivei de drumuri
				int []lineNew = {curent.row , curent.row+1 , curent.row, curent.row-1};//indicii liniilor pentru vecini
				int []collumnNew = {curent.collumn+1 , curent.collumn,curent.collumn -1 ,curent.collumn};//indicii coloanelor pentru vecini
				int rand = curent.row;
				int col = curent.collumn;
				int i =0; ;
				while(i<=3){
					//aici ii verific vecinii

					//iau indicii unui vecin la intamplare
					int icurent = lineNew[i];
					int jcurent = collumnNew[i];

					if (icurent<dimension && jcurent<dimension){
						if (icurent>=0 && jcurent>=0){
							if (MatriceFinala[icurent][jcurent] == -1 || MatriceFinala[icurent][jcurent] == 0){
								//nu ma intereseaza cazul killerilor-momentan 
								//de aceea iau casutele din matrieca finala care au nimic sau killer
								if ((MatriceFinala[icurent][jcurent] > MatriceFinala[rand][col] + 1 || (MatriceFinala[icurent][jcurent] == -1))) {

									MatriceFinala[icurent][jcurent] = MatriceFinala[rand][col] + 1; 
									listaPaznici.add(new Killer(icurent, jcurent));	
									//tinem minte killerul asta
								}
							}
							else{
								System.out.println("Aici ai killer");
							}
						}
					}
					i++;
				}

			}

		}
		/*	-1 -1 -1 -2  2  3  4  5
			-2 -1 -1 -2  1  2 -2  6
			8 -2 -2  1  0  1  2 -2
			7  6 -2  2 -2  2 -2  6
			6  5  4  3  4  3  4  5
			6  5  4  3  4  4  5  6
			-2 -2 -2  2  3  4 -2 -2
			2  1  0  1  2  3  4  5
		*/
		System.out.println();
		System.out.println("Matricea finala are forma:");
		for ( int i = 0 ; i<8;i++){
			for (int j = 0 ; j< 8 ; j++){
				System.out.print(MatriceFinala[i][j] + " ");
			}
			System.out.println();
		}
	}
}
