/**
 * Proiectarea Algoritmilor, 2014
 * Lab 8: Drumuri minime
 *
 * @author 	Radu Iacov
 * @email	radu.iacob23@gmail.com
 */

package minCost;

import graph.Graph;
import graph.Node;
import graph.Pair;

import java.util.ArrayList;

public class RoyFloyd {

	Integer cost[][];
	Node    detour[][];

	public static final int INF = Integer.MAX_VALUE;

	public RoyFloyd( int nodeCount )
	{
		cost   = new Integer[ nodeCount ][ nodeCount ];
		detour = new Node[ nodeCount ][ nodeCount ];
	}

	/**
	 * Given a graph with either positive or negative cost edges,
	 * compute the minimum path cost between every node as well
	 * as the 'next-hop' node for every such node. [3p] 
	 * 
	 * Expected complexity: O( V^3 )
	 * where V - num vertices in graph
	 * 
	 * Hint (useful API): 
	 * dumpCostMatrix();
	 * graph.getEdges(node)
	 * 
	 * Don't forget to initialize the cost and detour matrixes!
	 * 
	 * @param graph
	 */

	public void computeRoyFloyd( Graph graph )
	{
		resetCostMatrix();
		resetDetourMatrix();

		ArrayList<Node> nodes = graph.getNodes();
		int nodeCount = graph.getNodeCount();

		for (int i = 0; i < nodeCount; i++) {
			for (int j = 0 ; j < nodeCount; j++) {
				RoyFloyd.this.cost[i][j] = graph.getCost(nodes.get(i), nodes.get(j));
				RoyFloyd.this.detour[i][j] = null;
			}
		}

		for (int k = 0; k < nodeCount; k ++) {
			for (int i = 0; i < nodeCount; i++) {
				for (int j = 0 ; j < nodeCount; j++) {
					if (RoyFloyd.this.cost[i][j] > RoyFloyd.this.cost[i][k] + 
							RoyFloyd.this.cost[k][j]) {
						RoyFloyd.this.cost[i][j] = RoyFloyd.this.cost[i][k] + 
								RoyFloyd.this.cost[k][j];
						RoyFloyd.this.detour[i][j] = nodes.get(k);
					}
				}
			}
		}

		for (int i = 0; i < nodeCount - 1; i++) {
			for (int j = i + 1 ; j < nodeCount; j++) {
				PrintMinPath(graph, nodes.get(i), nodes.get(j));
			}
		}
		dumpCostMatrix();
	}

	/**
	 * Print the path with minimum cost between two given
	 * nodes, based on the detour matrix computed previously [1p]
	 * 
	 * @param node1
	 * @param node2
	 */

	public void PrintMinPath( Graph graph, Node node1, Node node2 )
	{
		System.out.print("Best route between ");
		System.out.println( node1.getCity() + " and " + node2.getCity() );

		graph.reset();
		node1.visit();
		int i = node1.getId();
		int j = node2.getId();
		//Hint: You may use PivotSanityCheck to retrieve
		//      the "next" node on the path from node1 to node2

		if (RoyFloyd.this.detour[i][j]==null){
			System.out.println("Nu exista asa ceva ma.");
			return;
			}
		ArrayList<Node> drum = new ArrayList<Node>();
		Node k = RoyFloyd.this.detour[i][j];
		while (k!=null){
			drum.add(k);
			 k = RoyFloyd.this.detour[i][k.getId()];
			
		}
		drum.add(node2);
		 ArrayList<Node> nodes = graph.getNodes();
         for (int t = drum.size()-1; t >= 0; t--)
                 System.out.print(nodes.get(drum.get(t).getId()).getCity() + " ");

		System.out.println("cOSTUL TOTAL: " + cost[node1.getId()][node2.getId()]);
	}

	/**
	 * 
	 * @param node1
	 * @param node2
	 * @return
	 */

	private Node PivotSanityCheck( Graph graph, Node node1, Node node2 )
	{
		int id1 = node1.getId();
		int id2 = node2.getId();

		if( id1 == id2 ){
			throw new RuntimeException("Path from node " + node1.getCity() + " to itself requested");
		}

		Node pivot = detour[id1][id2];
		if( pivot == null ){
			throw new RuntimeException("No path connects " + node1.getCity() + " to " + node2.getCity() );
		}

		if( pivot.isVisited() ){
			throw new RuntimeException("Cycle Detected");
		}
		pivot.visit();

		return pivot;
	}

	public void dumpCostMatrix()
	{
		for( int i = 0; i < cost.length; ++i )
		{
			for( int j = 0; j < cost[i].length; ++j ){
				System.out.print( cost[i][j] + " ");
			}
			System.out.print("\n");
		}

		System.out.println("\n");
	}

	public void resetCostMatrix()
	{
		for( int i = 0; i < cost.length; ++i ){
			for( int j = 0; j < cost[i].length; ++j ){
				cost[i][j] = INF;
			}
		}	
	}

	public void resetDetourMatrix()
	{
		for( int i = 0; i < detour.length; ++i ){
			for( int j = 0; j < detour[i].length; ++j ){
				detour[i][j] = null;
			}
		}
	}

}
