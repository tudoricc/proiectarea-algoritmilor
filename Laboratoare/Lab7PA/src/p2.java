/**
 * Proiectarea Algoritmilor, 2013
 * Lab 7: Aplicatii DFS
 * 
 * @author 	Radu Iacob
 * @email	radu.iacob23@gmail.com
 */



import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;


public class p2 {

	/*
	 * Afiseaza componentele biconexe, salvate in stiva
	 */
	
	static void print_biconex_component( Graph g, Node n1, Node n2 )
	{
		System.out.println("Componenta biconexa: ");
		
		HashSet< Node > comp = new HashSet< Node >();
		
		Pair< Node, Node > aux;
		
		do
		{
			aux = g.edge_stack.pop();
			
			comp.add( aux.getFirst() );
			comp.add( aux.getSecond() );
			
		}while( aux.getFirst() != n1 || aux.getSecond() != n2 );
		
		System.out.println( comp );
	}
	
	/*
	 * Useful API:
	 * 
	 * graph.edge_stack
	 * graph.muchii_critice
	 * graph.puncte_de_articulatie
	 * graph.get_edges
	 * 
	 * node.level
	 * node.lowlink
	 * node.in_stack
	 * 
	 */

	static void dfs_biconex( Graph g, Node node, int current_level, int father_id )
	{
		ArrayList< Node > childs = new ArrayList< Node >();
		
		/*
		 * TODO: Initializati level si lowlink
		 */
		node.level=current_level;
		node.lowlink = current_level;
		current_level++;
	/*	pentru fiecare (v, u) din E
		daca u nu este parinte
			daca idx[u] nu este definit
				dfsB(G, u, v)
				low[v] = min(low[v], low[u])
				daca (low[u] > idx[v])
					(v, u) este muchie critica
			altfel
				low[v] = min(low[v], idx[u])*/
		/*
		 * TODO: Parcurgere recursiva a copiilor nevizitati
		 */
			for ( int i = 0 ; i < g.get_edges(node).size();i++){
				Node copil = g.get_edges(node).get(i);
				if(!copil.visited()){
					childs.add(copil);
					g.edge_stack.push(new Pair(node,copil));
					dfs_biconex(g, copil , current_level, node._id);
					node.lowlink = Math.min(node.lowlink,copil.lowlink);
					if(node.level<copil.lowlink){
						g.muchii_critice.add(new Pair (node,copil));
					}
					if(copil.lowlink >= node.level){
	                    print_biconex_component(g, node, copil);
	                }
				}
				else{
					node.lowlink = Math.min(node.lowlink, copil.level);
				}
			}
		/*
		 * TODO: Identifica daca nodul este punct de articulatie
		 */
			if (father_id == -1){
	        	if (childs.size() >= 2){
	        		g.puncte_de_articulatie.add(node);
	        	}
	        }
	        else{
	            int ok = 0;
	             for(int i = 0; i < childs.size(); i++){
	               Node u = childs.get(i);
	               if(u.lowlink >= node.level){
	                   ok = 1;
	               }
	               
	             }
	             if( ok == 1){
	                    g.puncte_de_articulatie.add(node);
	               }
	        }
	}
	
	/*
	 * Descompune graful in componente biconexe
	 *
	 * Useful API:
	 * 
	 * g.get_nodes()
	 * node.visited()
	 */

	static void ComponenteBiconexe( Graph g )
	{
		g.reset();
		
		/*
		 * TODO: Parcurgere df pentru identificarea componentelor biconexe
		 */
		Node nod ;
		int current = 0 ;
		for ( int i = 0 ; i<g.get_nodes().size();i++){
			nod = g.get_nodes().get(i);
			if (!nod.visited()){
				dfs_biconex(g, nod, current, -1);
			}
		}
		System.out.println("\nPuncte. de articulatie: \n" + g.puncte_de_articulatie );
		System.out.println("\nMuchii critice: \n" + g.muchii_critice );
	}
	
	final static String PATH = "./res/test02";
	
	public static void main( String... args ) throws FileNotFoundException
	{
		Scanner scanner = new Scanner(new File(PATH));
		int test_count = scanner.nextInt();
		
		while( test_count-- > 0 )
		{
			Graph g = new Graph( Graph.GraphType.UNDIRECTED );	
			g.readData( scanner );
			System.out.println(g);
			ComponenteBiconexe(g);
		}
		
		scanner.close();
	}
	
	
	
}

