package Final;
import java.io.*;
import java.util.*;


public class Pitiprim {
	public ArrayList<Long> rightTruncnumbers;
	public ArrayList<Long> numbers;
	public ArrayList<Long> digits;
	public long no_numbers;
	public ArrayList<Long> numberPermute;
	public long maxr;
	public long MAXN = 100000000;
	public FileWriter fileout ;

	public Pitiprim() throws IOException{
		rightTruncnumbers = new ArrayList<Long>();
		numbers = new ArrayList<Long>();
		digits =  new ArrayList<Long>();
		numberPermute= new ArrayList<Long>();
		fileout = new FileWriter("pitiprim.out");
		maxr=0;
		rightTruncnumbers.add((long)2);
		rightTruncnumbers.add((long)3);
		rightTruncnumbers.add((long)5);
		rightTruncnumbers.add((long)7);
		rightTruncatable(2);
		rightTruncatable(3);
		rightTruncatable(5);
		rightTruncatable(7);
		//rightTruncatable(7);
	}


	public void read(BufferedReader fis) throws NumberFormatException, IOException{
		long numar = 0;

		no_numbers= Integer.parseInt(fis.readLine());

		for ( int i = 0 ; i < no_numbers ; i++){
			numar = Long.parseLong(fis.readLine());
			numbers.add(numar);
		}

	}
	public void rightTruncatable(long n)
	{
		long  nn;
		int i;
		long d[] = {1,3,7,9};

		if (n > maxr) maxr = n;
		if (n < MAXN / 10){
			for (i = 0; i < 4; i++)
			{
				nn = n*10 + d[i];
				if (isPrime(nn)!=0) {
					rightTruncnumbers.add(nn);
					rightTruncatable(nn);
				}
			}
		}
	}

	public long isPrime(long n)
	{
		long p;
		for (long i = 2 ;i<Math.sqrt(n);i++){
			if (n%i==0)
				return 0;
		}
		return 1;
	}
	public ArrayList<Long> getNoOfDigits(ArrayList<Long> digits){
		ArrayList<Long>occurences = new ArrayList<Long>();
		long ones = 0 ;
		long threes = 0;
		long sevens = 0 ;
		long nines = 0;
		long twos = 0;
		for ( int i = 0 ; i < digits.size();i++){

			if (digits.get(i) == 1) {
				ones++;
			}
			else if (digits.get(i)==2) {
				twos++;
			}
			else if (digits.get(i)==3) {
				threes++;
			}
			else if (digits.get(i)==7) {
				sevens++;
			}
			else if (digits.get(i)==9) {
				nines++;
			}

		}
		occurences.add(ones);
		occurences.add(twos);
		occurences.add(threes);
		occurences.add(sevens);
		occurences.add(nines);
		return occurences;
	}
	public ArrayList<Long> getDigits (long number){
		ArrayList<Long>digits = new ArrayList<Long>();
		long copyNo = number;
		while (copyNo > 0){
			long cifra = copyNo%10;
			digits.add(cifra);
			copyNo/=10;
		}
		return digits;
	}
	public boolean hasDigit(ArrayList<Long>digits , ArrayList<Long> original){
		boolean res=true;
		for ( int i = 0 ; i<digits.size();i++){
			if (!original.contains(digits.get(i))){
				res=false;
			
			}
		}

		return res;
	}
	public long checkPitiprim(long number) throws IOException{
		long copyNo=number;
		long currentNumber;
		ArrayList<Long>noDigitsNumber = new ArrayList<Long>();

		ArrayList<Long>digitsNumber = new ArrayList<Long>();
		ArrayList<Long>digitsRightTruncNumbers = new ArrayList<Long>();
		ArrayList<Long>noDigRightTrunc = new ArrayList<Long>();
		ArrayList<Long>noDigitsRightTruncNumbers = new ArrayList<Long>();
		ArrayList<Long>numbers = new ArrayList<Long>();
		ArrayList<Long>copyRightTrunc = rightTruncnumbers;

		digitsNumber=getDigits(copyNo);
		noDigitsNumber=getNoOfDigits(digitsNumber);

		for (int i =0;i<copyRightTrunc.size();i++){
			/*noDigitsRightTruncNumbers = getNoOfDigits(rightTruncnumbers.get(i));
			 */
			currentNumber = copyRightTrunc.get(i);
			digitsRightTruncNumbers =getDigits(currentNumber);
			noDigitsRightTruncNumbers=getNoOfDigits(digitsRightTruncNumbers);

			if (noDigitsRightTruncNumbers.get(0)<=noDigitsNumber.get(0) &&
					noDigitsRightTruncNumbers.get(1)<=noDigitsNumber.get(1)	&&
					noDigitsRightTruncNumbers.get(2)<=noDigitsNumber.get(2) &&
					noDigitsRightTruncNumbers.get(3)<=noDigitsNumber.get(3) &&
					noDigitsRightTruncNumbers.get(4)<=noDigitsNumber.get(4)) {
				if (hasDigit(digitsRightTruncNumbers, digitsNumber) == true){
					numbers.add(currentNumber);
				}

			}

		}
		return(Collections.max(numbers));
	}
	public static void main(String [] args) throws IOException{
		Pitiprim a = new Pitiprim();
		BufferedReader fis = new BufferedReader (new FileReader ("pitiprim.in"));
		BufferedWriter file = new BufferedWriter (new FileWriter("pitiprim.out"));
		a.read(fis);
	
		ArrayList<Long>digits = new ArrayList<Long>();
		for ( int i = 0 ; i< a.rightTruncnumbers.size();i++){
			System.out.println(a.rightTruncnumbers.get(i));
		}
		for ( int i = 0 ; i<a.numbers.size();i++){
			long res = a.checkPitiprim(a.numbers.get(i));
			file.write(res+"\n");
		}

		file.close();
		fis.close();
	}

}
