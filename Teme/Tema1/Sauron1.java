import java.io.*;
import java.util.*;

import javax.swing.text.MaskFormatter;
import javax.swing.text.html.MinimalHTMLWriter;
public class Sauron1 {
	public   ArrayList<Integer> masterListInit= new ArrayList<Integer>();

	public  ArrayList<Integer> slaveListInit; //vectorul initial de sclavi
	public  ArrayList<Integer> masterListFinal; //asta m-am gandit sa fie acel vector de exceptii
	public  ArrayList<Integer> slaveListFinal;
	public  int dimension; //dimensiunea celor 2 vectori
	public Sauron1(BufferedReader fis) {
		try {
			dimension= read(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}

		masterListFinal = init_vector(dimension/2 -1);
		slaveListFinal = init_vector( dimension/2  );
	}

	/*
	 * Aceasta este functia pentru citire din fisier care imi memoreaza in cei doi vectori
	 * stapani si sclavii din fisier
	 */
	public int  read(BufferedReader fis)throws IOException{

		int nr_perechi = Integer.parseInt(fis.readLine());
		String perechi = "";
		int i =0;
		dimension  = nr_perechi;
		String pereche[]=new String[2];

		masterListInit = new ArrayList<Integer>(nr_perechi);
		slaveListInit = new ArrayList<Integer>(nr_perechi);
		masterListInit = init_vector(nr_perechi);
		slaveListInit = init_vector(nr_perechi);
		//System.out.println(nr_perechi+ "numar perechi");
		while ((perechi = fis.readLine())!=null){
			masterListInit.set(i,Integer.parseInt(perechi.split(" ")[0]));
			//System.out.println("ai intrat aici");
			slaveListInit.set(i,Integer.parseInt(perechi.split(" ")[1]));
			//System.out.println("i" + i);
			i++;

		}
		return dimension;
	}


	/*
	 * Metoda aceasta imi ia dintr-un vector minimul si mi-l pune intr-un vector
	 * Gandeste-te la metoda asta ca fiind pentru stapani
	 * (pe care nu ii mai refolosim dupa ce am gasit minimul)
	 */
	public int minim_stapani(ArrayList<Integer> vector , int first , int last,ArrayList<Integer> exception){
		int min=999999;
		for (int i = first ; i<last ; i++){
			if ((vector.get(i)<min)&&(!exception.contains(vector.get(i))))
				min = vector.get(i);
		}

		return min;
	}

	/*
	 * Aceasta metoda imi ia din vectorul meu minimul dar nu il pune in vectorul de esceptii
	 * Te gandesti la functia asta ca fiind cea pentru sclavi
	 */
	public int minim_sclavi(ArrayList<Integer> vector , int first , int last){
		int min=999999;
		for (int i = first ; i<last ; i++){
			if ((vector.get(i)<min)){
				min = vector.get(i);
			}

		}
		return min;
	}


	/* 
	 * Fac initializari de vector 
	 * Functie care sa ma ajute pentru construirea vectorilor finali;
	 */
	public ArrayList<Integer> init_vector(int dimension){
		ArrayList<Integer> vector = new ArrayList<>(dimension+1);
		//System.out.println("dimensiune: "+dimension);
		for (int i = 0 ; i<=dimension ; i++ ){
			vector.add(0);
		}
		return vector;
	}
	/*Suma care trebuie platita de sauron pe un vector este calculata
	 * folosind functia de mai jos
	 */

	public int sumOfVector(ArrayList<Integer>vector){
		int suma = 0;
		for (int i : vector)
			suma+= i;
		return suma;
	}

	/*
	 * Functie care m-a ajutat la debugging
	 */

	public void printVector(ArrayList<Integer> vector){
		for (int i :vector ){
			System.out.print(i+" " );

		}
		System.out.println();
	}

	/*
	 * Functia in care s-a intamplat cam tot ce sta la baza algoritmului meu
	 * Pasi:
	 * 1.Pun din prima pereche sclavul pentru ca prima pereche nu isi poate grupa
	 * stapanul cu un sclav
	 * 2.Citesc cate o pereche.
	 * 3.Aleg stapanul si il adaug cu minimul sclavilor obtinuti pana la el
	 * 4.Aleg sclavul si il adun cu minimul stapanilor dupa el
	 * 5.Daca suma stapanului este mai mare decat suma sclavului atunci aleg 
	 * din pereche citita sclavul si stapanul pentru care am obtinut acel minim
	 * [stapanii nu se refolosesc-odata ales nu mai poate fi ales din nou]
	 * 5.Daca suma stapanului este mai mica decat suma sclavului atunci aleg
	 * stapanul si il pun in vectorul final de stapani
	 * 
	 */
	public void prelucrare(){

		slaveListFinal.set(0,slaveListInit.get(0));

		//	System.out.println(i);
		int pos_sclav=1;
		int pos_stapan = 0;
		int suma_stapan = 0;
		int suma_sclav = 0;
		int now =1;
		int copyDimension = masterListInit.size();
		//System.out.println(copyDimension);
		for (int i = 1 ; i<masterListInit.size();i++){
			/*if (masterListFinal.contains(masterListInit.get(i))){
				i++;
			}*/
			if (i == masterListInit.size()-1)
				break;
			else{



				int min_sclavi = minim_sclavi(slaveListFinal, 0, pos_sclav);
				int min_stapani = minim_stapani(masterListInit, i, masterListInit.size(), masterListFinal);

				suma_stapan = masterListInit.get(i) + min_sclavi;
				suma_sclav  = slaveListInit.get(i)+ min_stapani;

				// asta ia chiar elementul cel bunSystem.out.println(masterListInit.indexOf(min_stapani)+i);.
				//System.out.println(min_stapani);
				if (suma_stapan>=suma_sclav){
					//ar trebui sa adaaug acest indice direct intr-un vector mai destept de indici
					//System.out.println(masterListInit.indexOf(min_stapani));;
					int index =masterListInit.indexOf(min_stapani);

					//sterg din lista initiala ca sa nu mai refolosesc acei indici
					masterListInit.remove(masterListInit.indexOf(min_stapani));
					slaveListInit.remove(index);

					//adaug in lista finala ce ma intereseaza
					masterListFinal.set(pos_stapan, min_stapani);
					slaveListFinal.set(pos_sclav,slaveListInit.get(i));

					//merg mai departe in cei 2 vectori
					pos_sclav++;
					pos_stapan++;
				}
				if ( suma_stapan <suma_sclav){
					int index  = masterListInit.indexOf(min_stapani);
					if(index>0){

						//adaug acel stapan la lista stapanilor finali
						masterListFinal.set(pos_stapan,masterListInit.get(i));


						slaveListFinal.set(pos_sclav, slaveListInit.get(index+1));
						//masterListInit.remove(i);
						//sterg din vectorul initial de stapani si sclavi
						masterListInit.remove(index);
						slaveListInit.remove(index);

						//merg ma ideparte in vector
						pos_sclav++;
						pos_stapan++;
					}
				}
				/*System.out.println("minimul este pentru sclav egal cu"+min_sclavi + " iar pentru stapan " + min_stapani);
				System.out.println(":suma minima pentru stapan este  " + suma_stapan + " iar pentru sclav" + suma_sclav);
				System.out.println("=========================================================");
				System.out.println();
				System.out.println();
				 */
			}
		}
	}

	public static void main(String[] args) throws IOException{
		BufferedReader fis = new BufferedReader (new FileReader("date.in"));
		BufferedWriter out = new BufferedWriter(new FileWriter("date.out"));
		Sauron1 a = new Sauron1(fis);
		a.prelucrare();
		a.slaveListFinal.set(a.dimension/2,0);
		String output = ""+ (a.sumOfVector(a.masterListFinal)+a.sumOfVector(a.slaveListFinal));
		out.write(output);
		out.close();

	}
}