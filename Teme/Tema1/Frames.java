import java.io.*;


public class Frames {

	private int n;
	private int m;
	private int k;

	public Frames(int n, int m, int k) {
		this.n = n;
		this.m = m;
		this.k = k;
	}



	public int[][] initRes() {
		int[][] res = new int[k + 1][k + 1];
		for (int i = 0; i < res.length; ++i) {
			res[i][i] = 1;
		}
		return res;
	}

	/*
	 *Initializarea matricei mele de recurenta 
	 */
	public int[][] initMatrix() {
		int[][] matrix = new int[k + 1][k + 1];
		int j = 0;

		for (int i = 0; i <= k; i++) {
			for (int j1 = 0; j1 <= k; j1++) {
				matrix[i][j1] = 0;
			}
		}
		for (int i = 1; i <= k; i++) {
			matrix[i][j] = 1;
			j++;
		}
		for (int i = 0; i <= k; i++) {
			matrix[i][k] = 1;
		}
		return matrix;
	}

	// Inmultesc matricele A si B. Operatiile sunt modulo 40009.
	private int[][] multiplyMatrix(int[][] A, int[][] B) {
		final int N = A.length;
		final int M = B[0].length;
		final int K = A[0].length;

		int[][] res = new int[N][M];

		for (int i = 0; i < k + 1; ++i) {
			for (int j = 0; j < k + 1; ++j) {
				for (int k = 0; k < K; ++k) {
					res[i][j] = (res[i][j] + A[i][k] * B[k][j] % 40009) % 40009;
				}
			}
		}
		return res;
	}

	/*
	 * Obtinerea ultimului elemnt in umra inmultirii unui vector
	 */

	public int multiplyMatrixVector(int[][] A, int[] v) {
		int suma = 0;
		for (int i = 0; i < k + 1; i++) {
			suma = (suma + v[i] * A[i][k]) % 40009;
		}

		return (suma);
	}

	/* Deoarece lucram cu numere foarte mari este nevoie
	ca aceste functii sa fie cat mai eficiente.
	La functia de ridicare la o putere a unei matrici ma folosesc
	de divide et impera 
	Daca  avem puterea 1 ,returnez A,daca este zero returnez Ik ,
	iar daca puterea este para returnez inmultirea matricei rezultata
	in urma ridicarii matricii A la puterea/2 cu ea insasi.
	Daca avem o putere impara folosim oarecum rezultatul de la par dar il
	mai inmultim odata cu matricea A*/
	private int[][] logPowMatrix(int[][] A, int[][] res, int p) {
		if (p == 1) {
			return A;
		}
		if (p == 0)
			return res;

		int[][] Mat = logPowMatrix(A, res, p / 2);
		if (p % 2 == 0) {
			return multiplyMatrix(Mat, Mat);
		}

		return (multiplyMatrix(Mat, multiplyMatrix(Mat, A)));

	}
	/*
	 *La fel ca la problema ridicarii unei matrice la o putere
	 *si aici avem de ridicat un numar[foarte mari] la o putere
	 *La fel ca la Matrice am folosit tot o abordare Divide et impera
	 *Daca puterea este 0 ,returnez 1;daca este 1 returnez numarul.
	 *Daca puterea este para returnez produsul intre 
	 *numar^(putere/2) * numar^(putere/2) iar pentru o putere impara 
	 *returnez numar^(putere/2) * numar^(putere/2) * numar^1
	 *
	 *Reminder:Numere mari => La fiecare inmultire %40009
	 */
	public int power(int a, int b) {
		if (b == 0)
			return 1;
		if (b == 1)
			return a;
		//memorez aceasta variabila ca sa nu imi dea StackOverflowError
		int y = power(a, b / 2) % 40009;

		if (b % 2 == 0) {
			return (y * y) % 40009;
		}

		return (y * y % 40009 * a % 40009) % 40009;
		/*mai sus am pus dupa y*y modulo pentru ca poate sari rezultatul de limita integer
		 * dupa a pentru ca inmultirea rezultatului dintre y si a poate si el sari de 
		 * limita integer
		 */
	}
	/*
	 * Initializez matricea mea de recurenta, matricea in care memorez 
	 * ridicarea la putere a acesteia  si matricea Ik
	 * Incep prin ridicarea matricei noastre de recurenta la puterea
	 * n+1 ,pentru a obtineal k lea termen cand m=1 .Ultimul element al
	 * matricei fiind acel termen,pentru a obtine pentru orice m ridic
	 * acest rezultat la puterea m prin functia mea de ridicare la putere
	 */
	public String generareRezultat() {
		String output = "";
		int[][] res = initRes();
		int[][] matrix = initMatrix();
		int[][] powMatrix = new int[k + 1][k + 1];
		powMatrix = logPowMatrix(matrix, res, n + 1);

		//pregatesc scrierea in fisier
		output = power(powMatrix[k][k], m) + "";
		return output;
	}

	/*Functie care m-a ajutat sa verific corectitudinea 
	 * ridicarii la putere a unei matrici
	 */
	public void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}
	//functie care m-a ajutat la afisarea unui vector
	public void printVector(int[] vector) {
		for (int i : vector) {
			System.out.print(i + " ");
		}
		System.out.println();
	}

	/*Citirea propriu zisa din fisierul date.in
	 * Citesc o linie si impart linia [split] in functie de spatiu
	 * line = n_m_k
	 * memorez fiecare lucru in variabila corespunzatoare
	 * 
	 */
	public static int[] read() {
		int[] argList = new int[3];
		BufferedReader fis;
		try {
			fis = new BufferedReader(new FileReader("date.in"));
			String linie = fis.readLine();
			String[] args = linie.split(" ");
			argList[0] = Integer.parseInt(args[0]);
			argList[1] = Integer.parseInt(args[1]);
			argList[2] = Integer.parseInt(args[2]);
			fis.close();
		} catch (IOException e) {
			System.out.println("File cannot be open");
			e.printStackTrace();
		}
		return argList;
	}
	/*
	 *Afisarea in fisierul de output "date.out 
	 */
	public static void write(String output) {
		System.out.println(output);
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter("date.out"));
			out.write(output);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		int[] in = read();

		if (in.length != 3) {
			System.out.println("Arguments weren't initialize corectly");
			System.exit(0);
		}

		Frames frames = new Frames(in[0], in[1], in[2]);
		String output = frames.generareRezultat();

		write(output);
	}
}